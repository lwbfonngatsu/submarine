import numpy as np
import cv2


class Detectors(object):
    def __init__(self):
        self.fgbg = cv2.createBackgroundSubtractorMOG2()

    def Detect(self, frame, train, counter=0):
        """Detect objects in video frame using following pipeline
            - Convert captured frame from BGR to GRAY
            - Perform Background Subtraction
            - Detect edges using Canny Edge Detection
              http://docs.opencv.org/trunk/da/d22/tutorial_py_canny.html
            - Retain only edges within the threshold
            - Find contours
            - Find centroids for each valid contours
        Args:
            frame: single video frame
            train: For use faces data from video to train model
            counter: For face number
        Return:
            centers: vector of object centroids in a frame
        """
        face_cascade = cv2.CascadeClassifier("face.xml")
        # Convert BGR to GRAY
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Face detection
        centers = []  # vector of object centroids in a frame
        faces = face_cascade.detectMultiScale(gray, 1.15, 5)
        x, y, w, h = 0, 0, 0, 0
        w_s, h_s = [], []
        for (x, y, w, h) in faces:
            x = int(x + (w/2))
            y = int(y + (h/2))
            b = np.array([[x], [y]])
            w_s.append(w)
            h_s.append(h)
            centers.append(np.round(b))
            if train:
                cv2.imwrite("faces_from_video/face_" + str(counter) + ".png", frame[y:y+h, x:x+w])
                print("Saved face_" + str(counter) + ".png")
                counter += 1
        # # Perform Background Subtraction
        # fgmask = self.fgbg.apply(gray)
        # # Detect edges
        # edges = cv2.Canny(fgmask, 50, 190, 3)
        # # Retain only edges within the threshold
        # ret, thresh = cv2.threshold(edges, 127, 255, 0)
        #
        # # Find contours
        # _, contours, hierarchy = cv2.findContours(thresh,
        #                                           cv2.RETR_EXTERNAL,
        #                                           cv2.CHAIN_APPROX_SIMPLE)
        # centers = []  # vector of object centroids in a frame
        # blob_radius_thresh = 8
        # # Find centroid for each valid contours
        # for cnt in contours:
        #     try:
        #         # Calculate and draw circle
        #         (x, y), radius = cv2.minEnclosingCircle(cnt)
        #         centeroid = (int(x), int(y))
        #         radius = int(radius)
        #         if radius > blob_radius_thresh:
        #             cv2.circle(frame, centeroid, radius, (0, 255, 0), 2)
        #             b = np.array([[x], [y]])
        #             centers.append(np.round(b))
        #     except ZeroDivisionError:
        #         pass

        # show contours of tracking objects
        # cv2.imshow('Tracking', frame)
        return centers, w_s, h_s, counter
