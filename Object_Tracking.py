import cv2
import copy
from Object_Detector import Detectors
from Tracker import Tracker
import numpy as np


def main(train=0):
    # Create opencv video capture object
    cap = cv2.VideoCapture('real_video1.mp4')

    # Create Object Detector
    detector = Detectors()

    # Create Object Tracker
    tracker = Tracker(160, 60, 5, 100)

    # Variables initialization
    track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                    (0, 255, 255), (255, 0, 255), (255, 127, 255),
                    (127, 0, 255), (127, 0, 127)]
    pause = False
    centers = []
    frame_count = 0
    # Infinite loop to process video frames
    counter = 0
    while cap.isOpened():
        # Capture frame-by-frame
        ret, frame = cap.read()
        if not ret:
            break
        # Make copy of original frame
        orig_frame = copy.copy(frame)

        if frame_count >= 30:
            frame_count = 0

        if train:
            _, _, _, counter = detector.Detect(frame, train, counter)
        # Detect and return centeroids of the objects in the frame
        else:
            if len(centers) == 0 or frame_count == 0:
                centers, ws, hs = detector.Detect(frame, train)

            # If centroids are detected then track them
            if len(centers) > 0:
                frame_count += 1
                # Track object using Kalman Filter
                tracker.Update(centers)
                # For identified object tracks draw tracking line
                # Use various colors to indicate different track_id
                # print(len(hs), len(tracker.tracks))
                h_s = int(sum(hs)/len(hs))
                w_s = int(sum(ws)/len(ws))
                for i in range(len(tracker.tracks)):
                    if len(tracker.tracks[i].trace) > 1:
                        for j in range(len(tracker.tracks[i].trace)-1):
                            # Draw trace line
                            x1 = tracker.tracks[i].trace[j][0][0]
                            y1 = tracker.tracks[i].trace[j][1][0]
                            x2 = tracker.tracks[i].trace[j+1][0][0]
                            y2 = tracker.tracks[i].trace[j+1][1][0]
                            clr = tracker.tracks[i].track_id % 9
                            center1 = (x1, y1)
                            x, y = int(center1[0] - (w_s/2)), int(center1[1] - (h_s/2))
                            cv2.rectangle(frame, (x, y), (x + w_s, y + h_s), track_colors[clr], 4)
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)), track_colors[clr], 4)
            # Display the resulting tracking frame
            cv2.imshow('Tracking', frame)

        # Display the original frame
        # cv2.imshow('Original', orig_frame)

        # Check for key strokes
        k = cv2.waitKey(1) & 0xFF
        if k == 27:  # 'esc' key has been pressed, exit program.
            break
        if k == 112:  # 'p' has been pressed. this will pause/resume the code.
            pause = not pause
            if pause is True:
                print("Code is paused. Press 'p' to resume..")
                while pause is True:
                    # stay in this loop until
                    key = cv2.waitKey(1) & 0xff
                    if key == 112:
                        pause = False
                        print("Resume code..!!")
                        break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # execute main
    main(train=1)
